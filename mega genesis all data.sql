-- MariaDB dump 10.17  Distrib 10.4.8-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: desafio_ripley
-- ------------------------------------------------------
-- Server version	10.4.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cuenta_tipo`
--

DROP TABLE IF EXISTS `cuenta_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuenta_tipo` (
  `id_cuentaTipo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_cuentaTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuenta_tipo`
--

LOCK TABLES `cuenta_tipo` WRITE;
/*!40000 ALTER TABLE `cuenta_tipo` DISABLE KEYS */;
REPLACE INTO `cuenta_tipo` VALUES (1,'corriente'),(2,'vista'),(3,'ahorro'),(4,'ahorro vivienda');
/*!40000 ALTER TABLE `cuenta_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destinatario`
--

DROP TABLE IF EXISTS `destinatario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destinatario` (
  `id_destinatario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `rut` varchar(8) DEFAULT NULL,
  `digitoVerificador` varchar(1) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `fono` varchar(25) DEFAULT NULL,
  `banco` varchar(100) DEFAULT NULL,
  `cuentaNumero` varchar(100) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `tipoCuenta` int(11) NOT NULL,
  PRIMARY KEY (`id_destinatario`),
  KEY `fk_DESTINATARIO_USUARIO` (`usuario_id`),
  KEY `fk_DESTINATARIO_CUENTA_TIPO` (`tipoCuenta`),
  CONSTRAINT `fk_DESTINATARIO_CUENTA_TIPO` FOREIGN KEY (`tipoCuenta`) REFERENCES `cuenta_tipo` (`id_cuentaTipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_DESTINATARIO_USUARIO` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destinatario`
--

LOCK TABLES `destinatario` WRITE;
/*!40000 ALTER TABLE `destinatario` DISABLE KEYS */;
REPLACE INTO `destinatario` VALUES (1,'alguien','11111111','1','alguien@nadie.cl','555-25265','shile','124562027',1,1),(2,'alguien api','11111111','2','alguien@api.cl','555-5521','alguienburgo','7826548923',1,2),(3,'alguien api','11111111','2','alguien@api.cl','555-5521','alguienburgo','7826548923',1,2);
/*!40000 ALTER TABLE `destinatario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro`
--

DROP TABLE IF EXISTS `registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro` (
  `id_registro` int(11) NOT NULL AUTO_INCREMENT,
  `instancia` datetime DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `destinatario_id` int(11) NOT NULL,
  PRIMARY KEY (`id_registro`),
  KEY `fk_REGISTRO_DESTINATARIO1` (`destinatario_id`),
  CONSTRAINT `fk_REGISTRO_DESTINATARIO1` FOREIGN KEY (`destinatario_id`) REFERENCES `destinatario` (`id_destinatario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro`
--

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;
REPLACE INTO `registro` VALUES (1,'2021-06-06 22:34:10',15990,2),(2,'2021-06-06 22:37:28',20000,3);
/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `contrasenna` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
REPLACE INTO `usuario` VALUES (1,'ripley','123');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'desafio_ripley'
--
/*!50003 DROP PROCEDURE IF EXISTS `spIns_destinatario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `spIns_destinatario`(_nombre VARCHAR(100),_rut VARCHAR(8),_digitoVerificador VARCHAR(1),_correo VARCHAR(50),_fono VARCHAR(25),_banco VARCHAR(100),_cuentaNumero VARCHAR(100),_usuario_id INT,_tipoCuenta INT)
BEGIN
INSERT INTO `desafio_ripley`.`destinatario`
(
	`nombre`,
	`rut`,
	`digitoVerificador`,
	`correo`,
	`fono`,
	`banco`,
	`cuentaNumero`,
	`usuario_id`,
	`tipoCuenta`
)
VALUES
(
	_nombre,
	_rut,
	_digitoVerificador,
	_correo,
	_fono,
	_banco,
	_cuentaNumero,
	_usuario_id,
	_tipoCuenta
);
SELECT
1				AS	INTERNAL_CODE,
'insertado'		AS	MESSAGE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spIns_registro` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `spIns_registro`(_monto INT,_destinatario INT)
BEGIN
INSERT INTO `desafio_ripley`.`registro`
(
`instancia`,
`monto`,
`destinatario_id`
)
VALUES
(
	NOW(),
	_monto,
	_destinatario
);

SELECT
1							AS	INTERNAL_CODE,
'transferencia realizada'	AS	MESSAGE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spVal_usuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `spVal_usuario`(_nombre VARCHAR(45),_contrasenna VARCHAR(250))
BEGIN
	IF NOT EXISTS (SELECT * FROM USUARIO WHERE nombre=_nombre AND contrasenna = _contrasenna)
	THEN
		SELECT 
			-1			AS INTERNAL_CODE,
            'no existe'	AS	MESSAGE;
	ELSE
	SELECT 
		0			AS 	INTERNAL_CODE,
        'existe'	AS	MESSAGE,
        id_usuario	AS	id,
        nombre		AS	nombre
	FROM
		USUARIO
	WHERE
		nombre=_nombre 
        AND 
        contrasenna = _contrasenna;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-07  1:57:45
